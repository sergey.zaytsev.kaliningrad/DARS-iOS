//
//  DARSApp.swift
//  DARS
//
//  Created by Сергей Зайцев on 19.07.2022.
//

import SwiftUI

@main
struct DARSApp: App {
    var body: some Scene {
        WindowGroup {
            Auth()
        }
    }
}
