//
//  InterScreen.swift
//  DARS
//
//  Created by Сергей Зайцев on 27.07.2022.
//

import SwiftUI

struct InterScreen: View {
    var body: some View {
        NavigationView {
            ColorManager.backgroundOrange
                    .ignoresSafeArea()
                    .overlay(
                        VStack{
                            Text("DARS")
                                .font(.largeTitle)
                                .foregroundColor(.white)
                                
                        }
                        )
        }
}
}

struct InterScreen_Previews: PreviewProvider {
    static var previews: some View {
        InterScreen()
    }
}
